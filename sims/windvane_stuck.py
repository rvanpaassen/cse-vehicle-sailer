#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 18:27:38 2020

@author: repa
"""


if self.first:
    import time
    self.time = time.time
    self.t0 = self.time()
    self.gamma = 0
    self.delay = 60
    self.first = False


# hit after 80 seconds
t = self.time()
if t > self.t0 + self.delay and \
    t < self.t0 + 2*self.delay:
    if gamma > self.gamma + 15:
        self.gamma = gamma - 3
    elif gamma < self.gamma - 12:
        self.gamma = gamma + 4
        
    #back in normal range
    while self.gamma < -180.0:
        self.gamma += 360.0
    while self.gamma > 180.0:
        self.gamma -= 360.0
else:
    self.gamma = gamma
    
gamma = self.gamma

#code2
