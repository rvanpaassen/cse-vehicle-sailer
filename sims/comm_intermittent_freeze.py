#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 18:27:38 2020

@author: repa
"""


if self.first:
    import time
    import random
    import copy
    self.time = time.time
    self.random = random.random
    self.deepcopy = copy.deepcopy
    self.tfreeze = self.time() + (0.5 + self.random())*30
    self.frozen = False
    self.first = False


t = self.time()
if self.frozen:
    others_position = self.othersfrozen
    if t > self.tfreeze:
        #print("unfreezing")
        self.tfreeze += (0.5 + self.random())*20
        self.frozen = False
else:
    if t > self.tfreeze:
        #print("freezing")
        self.othersfrozen = self.deepcopy(others_position)        
        self.tfreeze += (0.5 + self.random())*35
        self.frozen = True
        
#code2
