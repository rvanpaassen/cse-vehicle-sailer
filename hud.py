#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 15:16:21 2019

@author: repa
@licence: GPL-v3.0
"""

# Panda3D imoprts
from direct.showbase.DirectObject import DirectObject
from direct.gui.DirectGui import DirectFrame, OnscreenText, DirectSlider
from panda3d.core import \
        TextNode, LineSegs, TextNode, Vec3, LColor, NodePath, Camera, \
        OrthographicLens
from math import pi, sin, cos
from numpy import degrees

# LineSegs

class Hud(DirectFrame):

    '''
    Display important parameters and create onscreen controls

    Uses DirectGui elements and text/line displays to provide information
    to the craft's operator. This is to be extended/developed into a
    EID displays by the students of AERSP 597, Section 011, 2019/2020
    '''

    def __init__(self, base, marklist: list) -> None:

        '''
        Create a new hud

        @param base   A `ShowBase` object, used to access the Panda3d window
        '''

        # make a 2d overlay of the window, if e.g. your screen
        # aspect ratio is 3/4, this will create a coordinate system
        # with bottom left at (-4/3, -1), and top right at (4/3, 1)
        dr = base.winList[0].makeDisplayRegion()
        # ask that it is drawn last, over all other elements there
        dr.setSort(20)

        # create the camera, 2D orthographic
        myCamera2d = NodePath(Camera('myCam2d'))
        lens = OrthographicLens()
        ar = base.getAspectRatio()
        if ar > 1:
            lens.setFilmSize(2*ar, 2)
        else:
            lens.setFilmSize(2/ar, 2)
        lens.setNearFar(-1000, 1000)
        myCamera2d.node().setLens(lens)

        # configure
        myRender2d = NodePath('myRender2d')
        myRender2d.setDepthTest(False)
        myRender2d.setDepthWrite(False)
        myCamera2d.reparentTo(myRender2d)
        dr.setCamera(myCamera2d)

        # example of a colored, fixed frame
        self.fr = DirectFrame(
                pos=Vec3(0.25,-1.0),
                frameSize = (0, 1.0, 0, 0.4),
                frameColor = (0, 0, 0, 0.5))

        # vehicle speed display, fixed (easy) number
        self.speed_display = OnscreenText(
                text="0.0 kts", pos=Vec3(0.1, 0.45), scale=0.08,
                fg=(0,1,0,0.6), align=TextNode.ACenter,
                parent=self.fr)

        # gui element for the tiller (steer the rudder)
        self.tiller_gui = DirectSlider(
            pos=Vec3(0.0, -0.9), scale=0.2,
            value=0.0, range=(-0.5,0.5), pageSize=0.05)

        # gui element for the mainsheet (control the sail)
        self.mainsheet_gui = DirectSlider(
            pos=Vec3(-0.8, -0.9), scale=0.2,
            value=1.0, range=(0.05, 1.0), pageSize=0.05)

        # example of a drawn instrument. Panda3d uses scene graph
        # techonology. Think of each graphical element as a node on
        # a branched tree. Each of the nodes can be scaled, rotated,
        # translated.

        # The compass consists of a LineSegs object with compass rose lines,
        # and we will combine that with N, E, S, W text labels
        # since we are doing 2d, only the first two element of vectors need
        # to be filled (x and y, z becomes zero).
        # Use moveTo and drawTo just like a pen, and if desired change color
        # and thickness along the way
        rose = LineSegs("rose")
        rose.setColor(LColor(1, 1, 1, 1))
        rose.setThickness(2.0)

        # compass rose lines
        for i in range(0,360,90):
            pnt = Vec3(sin(i/180*pi), cos(i/180*pi))
            rose.moveTo(pnt*0.75)
            rose.drawTo(pnt)
            for j in range(10,90,10):
                pnt = Vec3(sin((i + j)/180*pi), cos((i+j)/180*pi))
                rose.moveTo(pnt*0.85)
                rose.drawTo(pnt)

        # create and connect this to a new compass rose "node"
        self.compass_rose = NodePath("compass rose")
        self.compass_rose.attachNewNode(rose.create())

        # now connect the compass itself to the 2d window
        self.compass_rose.reparentTo(myRender2d)

        # now add the compass directions to the rose. These are (fixed)
        # TextNode objects
        # if you are new to Python, note that it can iterate over anything
        # iterable. Here we have a list (in []), with pairs ('tuples') of
        # data in it, each time a label and an angle
        # These are unpacked into l and angle variables
        for l, angle in [("N", 0), ("E", 90),
                         ("S", 180), ("W", 270)]:

            # create the textnode object & set the text
            tn = TextNode("label " + l)
            tn.setText(l)

            # create an additional node in the center of the compass
            piv = NodePath("pivot " + l)

            # create & link the text node to the center pivot
            tnp = piv.attachNewNode(tn)

            # link the pivot point to the compass rose as a whole
            piv.reparentTo(self.compass_rose)

            # scale text, and offset relative to the pivot
            tnp.setScale(0.2)
            tnp.setPos(Vec3(-0.1*tn.getWidth(), 0.45))

            # now rotate the pivot
            piv.setHpr(0, 0, angle)

        # scale the combined compass & put it in the corner
        self.compass_rose.setScale(0.2)
        self.compass_rose.setPos(Vec3(0.8,-0.8))

        # list of race marks
        # (type: '+'/'-' : round clockwise, counterclockwise
        #        'a'     : avoid (penalty)
        #        's'     : start
        #        'f'     : finish
        #  name:         : label for mark
        #  info:         : textual info
        #  xy            : position)
        self.marklist = marklist

    def update(self, x, y, psi, V, psiw, Vw, ds, others, race_events=None):
        '''
        Update the information on the displays

        Parameters
        ----------
        x : float [m]
            X location (North from center of the map) of the vehicle.
        y : float [m]
            Y location (East from center of the map) of the vehicle.
        psi : float [deg]
            Heading, in degrees.
        V : float [kts]
            Vehicle's speed over the ground.
        psiw : float [deg]
            relative direction of the wind, with respect to the vehicle.
            0 means wind coming from the bow (front), ranges between -180 and
            180
        Vw : float [kts]
            Relative wind speed, as would be measured by an anemometer in
            the mast.
        ds : float [deg]
            mainsheet angle, degrees, indicates how the boom/jib has moved.
        others : sequence of (...)
            Location of the other players in the field.
        race_events : sequence of (str, int, float)
            Notifications on race progress. The string indicating the event
            is 'R'; for rounding, 'P'; for penalty, 'F'; for finish.
            The integer indicates the rounded mark; 0=start
            the float indicates the elapsed time.

        Returns
        -------
        float [deg]
            Tiller commanded angle.
        float [deg]
            Mainsheet commanded angle.
        '''

        # update the compass rotation, and set the speed text
        self.compass_rose.setHpr(0, 0, -psi)
        self.speed_display.text = "{:2.1f} kts".format(V)

        # return the control inputs as a pair of values
        return self.tiller_gui['value'], self.mainsheet_gui['value']

if __name__ == '__main__':

    # this section is only executed if the hud.py is directly started,
    # it is for testing
    #
    # normally hud.py is imported by skater.py, and the test is not
    # performed (because then __name__ will be 'hud', Python trick ;-)

    # additional imports
    from direct.showbase.ShowBase import ShowBase
    from direct.task import Task


    class TestHud(ShowBase):
        '''
        Test object to testdrive the hud with dummy sine inputs
        '''

        def __init__(self):
            '''
            Create a test object
            '''

            ShowBase.__init__(self)
            self.taskMgr.add(self.testupdate, "testupdate")
            self.scene = self.loader.loadModel("models/environment")
            self.scene.reparentTo(self.render)
            self.scene.setScale(0.25, 0.25, 0.25)
            self.scene.setPos(-8, 42, 0)
            self.hud = Hud(self)
            self.dashboard = DirectFrame(
                    pos=Vec3(-0.8, 0.4), frameSize = (0, 0.4, 0, 0.4),
                    frameColor=(1, 1, 1, 0.5))

        def testupdate(self, task):
            '''
            drive the hud, callback
            '''

            # generate some nonsense data
            psi = sin(0.2*task.time)*360
            x = sin(0.1*task.time)*10
            y = cos(0.1*task.time)*10
            V = 5 + sin(0.1*task.time)
            psiw = psi + 0.2*sin(0.1*task.time)
            Vw = 8 + sin(0.05*task.time)
            ds = degrees(sin(task.time))
            others = [ (-20 + 3*sin(0.2*task.time), 20 + 3*cos(0.2*task.time)),
                       ( 20 + 3*sin(0.2*task.time), 20 + 3*cos(0.2*task.time)) ]

            self.hud.update(x, y, psi, V, psiw, Vw, ds, others)

            return Task.cont

    # create and run
    testhud = TestHud()
    testhud.run()
