#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 09:57:59 2020

@author: repa
"""
import numpy as np
import json
try:
    from RestrictedPython import compile_restricted as compile
    from RestrictedPython import safe_builtins, limited_builtins, \
        utility_builtins
except ImportError:
    print("Cannot import RestrictedPython")

basecode = """
class InstrumentPhysics:
    def __init__(self, hud):
        self.hud = hud
        self.first = True

    def update(self, x, y, psi, V,
               gamma, Vw, ds,
               others_position, eventlist):

        # code goes here, use self.first to init
        {code1}

        # call hud
        tiller, mainsheet = self.hud.update(
            x, y, psi, V, gamma, Vw, ds,
            others_position, eventlist)
        {code2}
        return tiller, mainsheet

"""


def createInstrumentPhysics(hud, code1, code2=''):
    '''
    Create an object that modifies the presented instrumentation
    or the control of the iceboat, depending on a server-supplied
    block with code.

    Assume the following variables to be available to the block
    code1:
        - self, self.first, to perform state based actions
        - x, y, psi, V: vehicle position, heading, speed
        - gamma, Vw: relative wind angle and speed
        - ds: current mainsheet angle
        - others-position: list with position competitors
        - eventlist: race event messages

    Angles are in degrees, positions in m

    The block code2 can optionally modify the tiller and mainsheet
    controls.

    Parameters
    ----------
    code1 : str
        Server supplied part of simulation, model instrument
        dynamics/failures.

    code2 : str
        Model control dynamics/failures.

    Returns
    -------
    Class/function object that provides the simulation.

    '''

    # prepare the code
    code = basecode.format(
        code1=code1.replace('\n', '\n        '),
        code2=code2.replace('\n', '\n        '))
    #print(code)
    
    # compile and create the class
    compiled = compile(code, 'instrumentphys', 'exec')
    localenv = dict(np=np, __builtins__=utility_builtins,
                    __metaclass__=type)
    exec(compiled, localenv, localenv)

    # create an instance and return
    obj = localenv['InstrumentPhysics'](hud)
    return obj


if __name__ == '__main__':

    allcode = """
x = 4
y = 5

#code2
phi = np.sin(0.2)*3
tiller = min(tiller, phi)"""

    class Hud:
        def __init__(self):
            self.t = 0

        def update(self, x, y, psi, V,
                 gamma, Vw, ds,
                 others_position, eventlist):
            print(
                f"x={x} y={y}, psi={psi} V={V} gamma={gamma} Vw={Vw}")
            self.t += 0.1
            return 20*np.sin(self.t), np.cos(self.t)
    hud = Hud()


    code1, code2 = allcode.split("#code2\n")
    twocode = json.dumps([code1, code2]).encode('ascii')
    print(twocode)
    c1, c2 = json.loads(twocode)
    inssim = createInstrumentPhysics(hud, c1, c2)

    for r in np.arange(0, 10, 0.2):
        print(inssim(np.sin(r), np.cos(r), 200,
                     4.5, 4, 5, 0.3, [], []))
